# Check diag ingest parameters for length zero
# This function checks if a value is length 0 and replaces it with the specified
# NA type if it is. It's used in places where we pull 0-length vectors from
# dataframes
help_check_diag_input <- function(x, na_type) {
  if (length(x) > 0 & class(na_type) %in% c("numeric", "integer")) {
    as.numeric(x)
  } else if (length(x) > 0 & class(na_type) == "character") {
    as.character(x)
  } else if (length(x) > 0 & class(na_type) == "logical") {
    as.logical(x)
  } else {
    na_type
  }
}

# Clean SQL query output to be useable in R
# This function formats date columns correctly and removes whitespace
# from the output of SQL queries
help_clean_sql_data <- function(x) {
  if (is.data.frame(x) & length(x) > 1) {
    x %>%
      mutate(
        across(contains("DATE"), ymd),
        across(where(is.character), ~ na_if(str_squish(.), ""))
      )
  } else if (is.data.frame(x) & length(x) == 1) {
    x %>%
      pull() %>%
      str_squish() %>%
      ifelse(length(.) == 0, NA_character_, .) %>%
      na_if("")
  } else if (is.vector(x) & length(x) != 0) {
    x %>%
      str_squish() %>%
      na_if("")
  } else {
    NA_character_
  }
}

# Run SQL queries using the input PIN and return cleaned output
# This is a wrapper function to safely handle SQL string interpolation
# The values specified are passed into whichever query file you specify
# and are properly quoted to prevent injection
help_run_query_file <- function(CCAODATA, file, val1 = NULL, val2 = NULL, val3 = NULL) {
  dbGetQuery(
    CCAODATA,
    glue_sql(readr::read_file(file), .con = CCAODATA)
  ) %>%
    help_clean_sql_data()
}

# Tables with list columns containing NULL or zero-length items cannot be
# rendered by shiny. This function will replace those values with NA
help_clean_tables <- function(df) {
  
  null_to_na <- function(x) as.character(ifelse(is.null(x) | length(x) == 0, NA, x))
  df %>% mutate(across(.fns = ~ sapply(.x, null_to_na)))
}

# Minor function to construct wiki links that can be used in table headers
help_link_wiki <- function(slug) {
  vcs_url <- Sys.getenv("VCS_URL")
  
  wiki_url <- ifelse(
    str_length(vcs_url) != 0,
    paste0(vcs_url, "/-/wikis/", slug),
    paste0(
      "https://gitlab.com/ccao-data-science---modeling",
      "/applications/shiny_app_pinval/-/wikis/", slug
    )
  )
  
  return(wiki_url)
}

# Help determine cut midpoints for histodot plot
help_get_cut_midpoints <- function(x, breaks) {
  intervals <- findInterval(x, breaks, all.inside = T)
  (breaks[intervals] + breaks[intervals + 1]) / 2
  (head(breaks, -1) + diff(breaks) / 2)[intervals]
}

# Replace negative/positive values with arrows and colors for easy viz.
format_num_change <- function(x, percent=TRUE, reverse_colors=TRUE, arrows=TRUE) {
  # if formatted as a percentage, convert to double for comparison
  d <- if (percent) as.double(sub('%$', '', x)) else x
  # colors chosen with https://davidmathlogic.com/colorblind/#%23D81B60-%231E88E5-%23FFC107-%23004D40
  # these are from the IBM color palette
  color_a <- "rgb(220, 38, 127)"
  color_b <- "rgb(120, 94, 240)"
  formatted = ifelse(
    d < 0.0, 
    paste0(
      sprintf("<span style='color:%s'>", ifelse(reverse_colors, color_a, color_b)), 
      if (arrows) as.character(icon("caret-down")) else "",
      " ",
      sub('.', '', x),
      "</span>"
    ),
    ifelse(
      d > 0.0, 
      paste0(
        sprintf("<span style='color:%s'> ", ifelse(reverse_colors, color_b, color_a)),
        if (arrows) as.character(icon("caret-up")) else "",
        " ",
        x,
        "</span>"
      ), 
      paste0(if (arrows) "<b>-</b> " else "", x)
    )
  )
  return(formatted)
}

# Insert a popup definition for key terms. Loads definitions from master CSV file for consistency.
# Uses the shinyBS package.
# 
# Note - these popups will not work in DataTables on mobile. 
# PINVAL automatically removes them for mobile, but they will show up on desktop.
definition <- function(key, icon="question-circle-o", ...) {
  definitions <- readr::read_csv(
      "data/definitions.csv",
      col_types = readr::cols()
    ) %>%
    filter(Key == key)
  validate(
    need(nrow(definitions) > 0, "No definition found."),
    need(nrow(definitions) == 1, "Duplicate or missing definition")
  )
  term <- htmltools::htmlEscape(as.character(definitions$Term), attribute=TRUE)
  def <- htmltools::htmlEscape(as.character(definitions$Definition), attribute=TRUE)
  return(popify(
    icon(icon, "popover-icon"), 
    term, 
    def,
    ...
  ))
}

# Wrapper for the tipify function, to make tooltips easier and consistent.
tooltip <- function(tip, icon="cogs", ...) {
  return(popify(
    icon(icon, "popover-icon"),
    paste(icon("lightbulb-o"), "Tip"),
    tip,
    ...
  ))
}
 
color_box <- function(color) {
  return(sprintf("<div class='color-box' style='background-color: %s;'></div>", color))
}

# generates a color based on a percentile value along a (linear) scale
color_palette <- function(percentile, colors) {
  palette <- colorRampPalette(colors)
  return(palette(101)[percentile+1])  
}
