
# Topline characteristics data table
# This table contains all the physical attributes of the target PIN
# If values are missing they are rendered by renderTable() as --
# Each 'bunch' of lines represents a single row of the output table
table_topline_chars <- function(diag_lst) {
  topline_chars_df <- tribble(
    ~x1, ~x2, ~x3, ~x4,

    "Address", diag_lst$diag_full_address,
    "Triad", diag_lst$diag_triad_name,

    "Township", diag_lst$diag_town_name,
    "Land Area", glue(scales::comma(diag_lst$diag_hd_sf), " SqFt", .na = NULL),

    "Neighborhood", as.character(diag_lst$diag_nbhd),
    "Improvement Area", glue(scales::comma(diag_lst$diag_bldg_sf), " SqFt", .na = NULL),

    paste(definition("property_class", placement="left"), "Property Class"), diag_lst$diag_class,
    "Age (Corrected)", glue(diag_lst$diag_age, " Years Old", .na = NULL),

    "Bedrooms", as.character(diag_lst$diag_beds),
    "Basement", glue(diag_lst$diag_bsmt, ", ", diag_lst$diag_bsmt_fin, .na = NULL),

    "Bathrooms", glue(diag_lst$diag_fbath, " Full / ", diag_lst$diag_hbath, " Half", .na = NULL),
    "Garage Size", diag_lst$diag_gar1_size,

    "Porch", diag_lst$diag_porch,
    "Facade", diag_lst$diag_ext_wall,

    "HVAC", glue(diag_lst$diag_heat, ", ", diag_lst$diag_air, .na = NULL),
    "Roof Material", diag_lst$diag_roof_cnst
  ) %>%
    help_clean_tables()

  return(topline_chars_df)
}


# Table displaying condo sales from inside the same building
# The target PIN is highlighted in red if it has sales that appear
table_condo_sales <- function(diag_lst, condos_sales_df) {

  # Create datatable object, the options here are passed to the javascript
  # datatable library
  condos_sales_df %>%
    mutate(PIN = replace(PIN, subject_pin, "This Property")) %>%
    datatable(
      rownames = FALSE,
      filter = "none",
      autoHideNavigation = TRUE,
      selection = "none",
      options = list(
        autoWidth = FALSE,
        scrollX = FALSE,
        searchHighlight = TRUE,
        columnDefs = list(
          list(targets = 0, width = "150px"),
          list(targets = 1, width = "90px"),
          list(targets = 4, visible = FALSE)
        )
      )
    ) %>%
    formatStyle(
      "subject_pin",
      target = "row",
      backgroundColor = styleEqual(c(0, 1), c("transparent", "#AA110050"))
    ) %>%
    formatCurrency(
      c("Sale Price"),
      digits = 0
    ) %>%
    formatPercentage(
      c("Percent Assessed"),
      digits = 2
    )
}


# Topline appeals data table, containing average reduction %, win rate, etc
table_topline_appeals <- function(diag_lst, appeals_df) {

  # Get the most recent appeal for both the board and CCAO
  ccao_most_recent <- appeals_df %>%
    arrange(desc(TAX_YEAR)) %>%
    filter(CCAO_REDUCE != 0) %>%
    slice(1)
  
  bor_most_recent <- appeals_df %>%
    arrange(desc(TAX_YEAR)) %>%
    filter(BOR_REDUCE != 0) %>%
    slice(1)
  
  # Manually construct a list of summary statistics to be used in the
  # construction of our table. Each stat is assigned to an element of a named
  # list
  appeals_sum <- lst(
    ccao_num_appeals = appeals_df %>%
      filter(TYPE %in% c("CCAO", "CCAO + BoR")) %>%
      nrow(),
    
    bor_num_appeals = appeals_df %>%
      filter(TYPE %in% c("BoR", "CCAO + BoR")) %>%
      nrow(),

    # Appeal win rate = num wins / num total appeals
    ccao_appeal_win_rate = appeals_df %>%
      summarize(win_rate = mean(CCAO_WINS, na.rm = TRUE)) %>%
      pull(win_rate),
    
    bor_appeal_win_rate = appeals_df %>%
      summarize(win_rate = mean(BOR_WINS, na.rm = TRUE)) %>%
      pull(win_rate),

    # Average reduction %, not including 0s
    ccao_avg_reduction_on_win = appeals_df %>%
      summarise(redc = mean(na_if(CCAO_REDUCE, 0), na.rm = TRUE)) %>%
      pull(redc),
    
    bor_avg_reduction_on_win = appeals_df %>%
      summarise(redc = mean(na_if(BOR_REDUCE, 0), na.rm = TRUE)) %>%
      pull(redc),
    
    # Most recent appeal reduction as a percent
    ccao_most_recent_reduction_percent = ccao_most_recent %>% pull(CCAO_REDUCE),
    bor_most_recent_reduction_percent = bor_most_recent %>% pull(BOR_REDUCE),

    # Most recent appeal reduction as a dollar amount
    ccao_most_recent_reduction_dollar = ccao_most_recent %>%
      mutate(dollar_reduction = (CERTIFIED - MAILED) * 10) %>%
      pull(dollar_reduction),
    
    bor_most_recent_reduction_dollar = bor_most_recent %>%
      mutate(dollar_reduction = (BOARD - CERTIFIED) * 10) %>%
      pull(dollar_reduction),

    # Most recent year that a reduction was received
    ccao_most_recent_reduction_year = ccao_most_recent %>% pull(TAX_YEAR),
    bor_most_recent_reduction_year = bor_most_recent %>% pull(TAX_YEAR)
    
  )

  # Manually contruct the topline table, each bunch of lines is a single row
  topline_appeals_df <- tribble(
    
    ~" ", ~"Assessor's Office", ~"Board of Review",

    glue("Num. Appeals Since ", diag_lst$diag_min_year),
    as.character(appeals_sum$ccao_num_appeals),
    as.character(appeals_sum$bor_num_appeals),
    
    glue("Appeal Win Rate Since ", diag_lst$diag_min_year),
    percent(appeals_sum$ccao_appeal_win_rate),
    percent(appeals_sum$bor_appeal_win_rate),
    
    glue("Avg. Reduction on Win Since ", diag_lst$diag_min_year),
    percent(appeals_sum$ccao_avg_reduction_on_win),
    percent(appeals_sum$bor_avg_reduction_on_win),
    
    "Most Recent Appeal Reduction Year", 
    as.character(appeals_sum$ccao_most_recent_reduction_year),
    as.character(appeals_sum$bor_most_recent_reduction_year),

    "Most Recent Appeal Reduction (%)",
    percent(appeals_sum$ccao_most_recent_reduction_percent),
    percent(appeals_sum$bor_most_recent_reduction_percent),

    "Most Recent Appeal Reduction ($)",
    dollar(appeals_sum$ccao_most_recent_reduction_dollar),
    dollar(appeals_sum$bor_most_recent_reduction_dollar)
  ) %>%
    rename(
      !!paste(definition("board", placement="top"), "Board of Review") := "Board of Review"
    ) %>%
    help_clean_tables()

  return(topline_appeals_df)
}


# Topline valuation data table
table_topline_valuation <- function(diag_lst, assmnt_df) {
  # Grab only the data of our PIN from the overall assessment data frame
  topline_valuation_df <- assmnt_df %>%
    filter(PIN == diag_lst$diag_pin_value) %>%
    arrange(YEAR) %>%
    mutate(
      m2a = format_num_change(percent((CERTIFIED - `FIRST PASS`) / `FIRST PASS`)),
      a2b = format_num_change(percent((`BOR RESULT` - CERTIFIED) / CERTIFIED)),
      yoy = format_num_change(percent((CERTIFIED - lag(CERTIFIED)) / CERTIFIED)),
      across(`FIRST PASS`:`BOR RESULT`, dollar)
    ) %>%
    select(
      Year = YEAR, 
      !!paste(definition("mailed", placement="top"), "Mailed") := `FIRST PASS`, 
      !!paste0(" ", as.character(icon("arrow-right"))) := m2a,
      !!paste(definition("certified", placement="top"), "Certified") := CERTIFIED, 
      !!as.character(icon("arrow-right")) := a2b,
      !!paste(definition("board", placement="top"), "Board") := `BOR RESULT`, 
      `Year-Over-Year Change (Certified)` = yoy
    ) %>%
    help_clean_tables()
  return(topline_valuation_df)
}


# Interactive table in the comps tab, highlights the corresponding PIN in the
# comps leaflet map on clicking a row
table_comps_map_ref <- function(diag_lst, comps_df, distance) {

  # Take all comps that lie within the distance specified, in this case the
  # distance input is from our input slider. Also clean up column formatting
  # and keep only the desired columns
  comps_map_ref_df <- comps_df %>%
    filter(COMP_DIST <= distance) %>%
    arrange(desc(COMP_DIST)) %>%
    distinct(COMP_PIN, .keep_all = TRUE) %>%
    mutate(
      COMP_PIN = ccao::pin_format_pretty(COMP_PIN),
      COMP_DIST = round(COMP_DIST, 2),
      TARG_FMV = diag_lst$diag_mryad_value,
      "Abs. Diff." = abs(TARG_FMV - ADJ_SALE_PRICE),
    ) %>%
    select(
      "Comparable PIN" = COMP_PIN, "Class" = COMP_CLASS,
      "Distance" = COMP_DIST,
      "Sale Date" = SALE_DATE, "Sale Price" = SALE_PRICE,
      "Time Adj. Sale Price" = ADJ_SALE_PRICE,
      "This Property's Fair Market Value" = TARG_FMV, "Abs. Diff."
    ) %>%

    # Create a datatable object that also controls the leaflet map
    datatable(
      rownames = FALSE,
      filter = "none",
      autoHideNavigation = TRUE,
      selection = "none",
      options = list(
        autoWidth = TRUE,
        scrollX = TRUE,
        searchHighlight = TRUE,
        columnDefs = list(
          list(targets = c(0, 1), orderable = FALSE),
          list(targets = 0, width = "100px"),
          list(targets = 3, width = "80px"),
          list(targets = c(0, 1, 3), className = "dt-center")
        )
      )
    ) %>%
    formatCurrency(
      c("Sale Price", "Time Adj. Sale Price", "This Property's Fair Market Value", "Abs. Diff."),
      digits = 0
    ) %>%
    formatRound(
      c("Distance"),
      digits = 2
    ) %>%
    formatString(c("Distance"), suffix = " mi.") %>%
    formatStyle(
      "This Property's Fair Market Value",
      backgroundColor = "#AA110050"
    )

  return(comps_map_ref_df)
}


# Simple table displaying the boundaries of the uniformity comps algorithm
table_unif_bounds <- function(diag_lst, unif_df) {

  # Manually grab the target PIN's characteristic data from unif_df
  uniformity_target_pin <- unif_df %>%
    filter(PIN == diag_lst$diag_pin_value) %>%
    distinct(PIN, .keep_all = TRUE)

  unif_dsqft <- uniformity_target_pin %>%
    pull(DOLLAR_SQFT)

  unif_bsqft <- uniformity_target_pin %>%
    pull(BLDG_SF)

  unif_lsqft <- uniformity_target_pin %>%
    pull(LND_SF)

  unif_age <- uniformity_target_pin %>%
    pull(AGE)

  # Manually construct a table that includes each characteristic that was used
  # to find uniformity comparables and its left and right bound
  # Each bunch is a row of the table
  uniformity_bounds_df <- tribble(
    ~"Characteristic", ~"Lower Bound", ~"This Property", ~"Upper Bound",

    "Price / Building Sq. ft.",
    dollar(unif_dsqft - (unif_dsqft * 0.5), 1),
    dollar(unif_dsqft, 1),
    dollar(unif_dsqft + (unif_dsqft * 0.5), 1),

    "Building Sq. ft.",
    comma(unif_bsqft - (unif_bsqft * 0.15), 1),
    comma(unif_bsqft, 1),
    comma(unif_bsqft + (unif_bsqft * 0.15), 1),

    "Land Sq. ft.",
    comma(unif_lsqft - (unif_lsqft * 0.15), 1),
    comma(unif_lsqft, 1),
    comma(unif_lsqft + (unif_lsqft * 0.15), 1),

    "Age",
    comma(unif_age - (unif_age * 0.2), 1),
    comma(unif_age, 1),
    comma(unif_age + (unif_age * 0.2), 1),

    "Ext. Wall",
    NA_character_, as.character(diag_lst$diag_ext_wall), NA_character_,
    
    "Neighborhood",
    NA_character_, as.character(diag_lst$diag_nbhd), NA_character_,
    
    "Class",
    NA_character_, as.character(diag_lst$diag_class), NA_character_
  ) %>%
    rename(
      !!paste("<span style='font-weight: normal'>Lower Bound</span><br>", as.character(icon("angle-double-left"))) := "Lower Bound",
      !!paste("<span style='font-weight: normal'>This Property</span><br>", as.character(icon("dot-circle-o"))) := "This Property",
      !!paste("<span style='font-weight: normal'>Upper Bound</span><br>", as.character(icon("angle-double-right"))) := "Upper Bound"
    ) %>%
    help_clean_tables()

  return(uniformity_bounds_df)
}


# Reference tables for uniformity comparables plot
# This table is by far the most complicated, as it has interactivity with other
# UI elements and conditional formatting
table_unif_comps_ref <- function(diag_lst, unif_df) {

  # Given our table of uniformity comparables we want to use the dictionary
  # in data/ to convert each characteristic's numeric code to its corresponding
  # character value. This is accomplished by converting CCAOSFCHARS to a long
  # data format and then joining the dictionary by code and name, then
  # converting it back to wide. The result is a wide dataframe with a row for
  # each PIN and columns for each characteristic
  uniformity_comps_ref_df <- unif_df %>%
    mutate(subject_pin = as.numeric(PIN == diag_lst$diag_pin_value)) %>%
    arrange(desc(subject_pin)) %>%
    select(
      subject_pin, PIN,
      "$/BSF" = DOLLAR_SQFT,
      BSF = BLDG_SF, LSF = LND_SF, AGE, "TOTAL AV" = ASSMNT_VAL,
      ROOF_CNST, FB = FBATH, HB = HBATH,
      AIR, HEAT, BSMT, BSMT_FIN, GAR1_SIZE
    ) %>%
    ccao::vars_recode(type = "short") %>%
    rename(ROOF = ROOF_CNST, GARG = GAR1_SIZE) %>%
    unite(BSMT, BSMT, BSMT_FIN, sep = " ") %>%
    mutate_at(
      vars(ROOF:GARG),
      list(M = ~ ifelse(.x == first(.x), 1, 0))
    ) %>%
    mutate(PIN = ccao::pin_format_pretty(PIN)) %>%
    mutate(PIN = replace(PIN, subject_pin, "This Property")) %>%
    rename(
      "Comparable PIN" = PIN,
      "Building area (sq.ft.)" = BSF,
      "Price per building sq.ft." = `$/BSF`,
      "Land area (sq.ft.)" = LSF,
      "Age (yrs.)" = AGE,
      "Total Assessed Value" = `TOTAL AV`,
      "Roof" = ROOF,
      "Full Baths" = FB,
      "Half Baths" = HB,
      "Air" = AIR,
      "Heat" = HEAT,
      "Basement" = BSMT,
      "Garage" = GARG
    ) %>%
    # Datatable object that creates the uniformity interactive table
    # Options are passed directly to the datatable JS library
    datatable(
      rownames = FALSE,
      filter = "none",
      autoHideNavigation = TRUE,
      selection = "none",
      options = list(
        autoWidth = TRUE,
        scrollX = TRUE,
        searchHighlight = TRUE,
        columnDefs = list(
          list(targets = c(0, 14:20), visible = FALSE),
          list(targets = c(1, 7:13), orderable = FALSE),
          list(targets = c(2:6), width = "10px"),
          list(targets = 1, width = "85px", className = "dt-center"),
          list(targets = 7, width = "60px")
        )
      )
    ) %>%
    # Conditional formatting based on _M indicator column created earlier
    # PINs with the same characteristic as the target will be green
    formatStyle(
      c("Roof", "Full Baths", "Half Baths", "Air", "Heat", "Basement", "Garage"),
      c("ROOF_M", "FB_M", "HB_M", "AIR_M", "HEAT_M", "BSMT_M", "GARG_M"),
      backgroundColor = styleEqual(
        c(0, 1),
        c("#ffdf99", "transparent")
      )
    ) %>%
    formatCurrency(
      c("Price per building sq.ft.", "Total Assessed Value"),
      digits = 0
    ) %>%
    # Conditional formatting to make the target PIN red
    formatStyle(
      "subject_pin",
      target = "row",
      backgroundColor = styleEqual(c(0, 1), c("transparent", "#AA110050"))
    ) %>%
    formatRound(
      c("Building area (sq.ft.)", "Land area (sq.ft.)"),
      digits = 0
    )

  return(uniformity_comps_ref_df)
}


# Reference table for history FMV plot
# Shows all CCAO appeals data. Comment columns are concatenated into a single
# column that shows the full comment of the analyst who processed the appeal
table_hist_fmv_ref <- function(diag_lst, appeals_df) {
  ccao_appeals_df <- appeals_df %>%
    filter(TYPE %in% c("CCAO", "CCAO + BoR")) %>%
    mutate(
      `Appeal Type` = "Assessor's Office",
      `Pre-Appeal Fair Market Value` = MAILED * 10,
      `Post-Appeal Fair Market Value` = CERTIFIED * 10,
      `Reduction $` = abs(CERTIFIED * 10 - MAILED * 10),
      `Reduction %` = CCAO_REDUCE,
      Description = DESCRIPTION,
      color_idx = "1"
    ) %>%
    select(Year = TAX_YEAR, `Appeal Type`:color_idx)
  
  bor_appeals_df <- appeals_df %>%
    filter(TYPE %in% c("BoR", "CCAO + BoR")) %>%
    mutate(
      `Appeal Type` = "Board of Review",
      `Pre-Appeal Fair Market Value` = CERTIFIED * 10,
      `Post-Appeal Fair Market Value` = BOARD * 10,
      `Reduction $` = abs(BOARD * 10 - CERTIFIED * 10),
      `Reduction %` = BOR_REDUCE,
      Description = NA_character_,
      color_idx = "2"
    ) %>%
    select(Year = TAX_YEAR, `Appeal Type`:color_idx)

  # using a custom container, a la https://rstudio.github.io/DT/
  sketch <- htmltools::withTags(table(
    class='display', 
    thead(
      tr(
        th(rowspan=2, 'Year'),
        th(rowspan=2, definition("appeal_type", placement="top"), "Appeal Type"),
        th(colspan=2, definition("fmv", placement="top"), "Fair Market Value"),
        th(colspan=2, 'Reduction'),
        th(rowspan=2, 'Description')
      ),
      tr(
        th('Pre-Appeal'),
        th('Post-Appeal'),
        th('$'),
        th('%')
      )
    )
  ))
  history_fmv_ref_df <- bind_rows(ccao_appeals_df, bor_appeals_df) %>%
    arrange(Year) %>%
    mutate(`Reduction %` = format_num_change(percent(`Reduction %`), arrows=FALSE, reverse_colors=TRUE)) %>%
    # Datatable object displaying appeals. Appeals which are granted are
    # highlighted in green, appeals which are not granted have no highlighting
    datatable(
      rownames = FALSE,
      filter = "none",
      autoHideNavigation = TRUE,
      selection = "none",
      container=sketch,
      options = list(
        autoWidth = TRUE,
        scrollX = TRUE,
        paging = FALSE,
        searching = FALSE,
        info = FALSE,
        columnDefs = list(
          list(targets = 6, sortable = FALSE),
          list(targets = 7, visible = FALSE)
        )
      ),
      escape=FALSE
    ) %>%
    formatCurrency(
      c("Pre-Appeal Fair Market Value", "Post-Appeal Fair Market Value", "Reduction $"),
      digits = 0
    ) %>%
    formatStyle(
      "Appeal Type", "color_idx",
      backgroundColor = styleEqual(c(1, 2), c("#7282b3", "#b2d3d6")),
    )

  return(history_fmv_ref_df)
}


# Table to display 288 outcomes
# Goal is to highlight changes in characteristics caused by 288s
table_h288s_changes <- function(diag_lst, h288s_df) {

  # Create a datatable that highlights characteristics that are nonzero in yellow
  h288s_changes_df <- h288s_df %>%
    select(
      Class = QU_CLASS, Year = YEAR, everything(),
      -QU_PIN, `Active 288s` = NUM_288S_ACTIVE
    ) %>%
    relocate(`Active 288s`, .after = Year) %>%
    mutate(
      across(QU_NUM_APTS:QU_GARAGE_SIZE, ~ .x != 0, .names = "{col}_cc"),
      Class = str_sub(Class, 1, 3)
    ) %>%
    ccao::vars_recode() %>%
    ccao::vars_rename(names_from = "addchars", names_to = "pretty") %>%
    datatable(
      rownames = FALSE,
      filter = "none",
      autoHideNavigation = TRUE,
      selection = "none",
      options = list(
        info = FALSE,
        paging = FALSE,
        searching = FALSE,
        autoWidth = TRUE,
        scrollX = TRUE,
        searchHighlight = TRUE,
        columnDefs = list(
          list(targets = 31:58, visible = FALSE)
        )
      )
    ) %>%

    # Conditional formatting based on _cc indicator column created earlier
    formatStyle(
      3:31,
      31:59,
      backgroundColor = styleEqual(
        c(0, 1),
        c("transparent", "#ffdf99")
      )
    )

  return(h288s_changes_df)
}
