mod_download_UI <- function(id) {
  ns <- NS(id)

  # UI element that displays once req() function is met. This is used to hide
  # the download button until the data to download is actually available
  uiOutput(ns("download_button"))
}


mod_download <- function(input, output, session, get_diag_lst, data, data_check, filename, label) {
  ns <- session$ns

  # Download handler function that outputs a name and a file to write
  # The name is the PIN value and name of the dataset
  # The file is whatever reactive dataframe gets passed to the main module
  output$download_handler <- downloadHandler(
    filename = function() {
      validate_no_msg(check_diag_lst(get_diag_lst()))
      validate_no_msg(data_check(data()))
      paste(get_diag_lst()$diag_pin_value, filename, sep = "_")
    },
    content = function(file) {
      req(data())
      readr::write_csv(data(), file)
    },
    contentType = "text/csv"
  )

  # Download button UI object. This only gets rendered after the data() passed
  # to the module becomes available
  output$download_button <- renderUI({
    validate_no_msg(check_diag_lst(get_diag_lst()))
    validate_no_msg(data_check(data()))
    downloadButton(
      ns("download_handler"),
      label = label
    )
  })
}
