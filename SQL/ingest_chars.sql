/* 

The purpose of this query is to gather correct characteristics data and
proration rates from the detail and characteristics tables

To do this, we must first join detail and chars by pin, year, and multicode,
then keep only the row that has the same class as the row in HEAD

*/

WITH DTC AS (
	SELECT 
		DT.PIN, DT.TAX_YEAR, DT_CLASS AS CLASS, DT_MLT_CD, DT_PER_ASS, DT_CDU,
		CH.TYPE_RESD, CH.[USE], CH.APTS, CH.EXT_WALL, CH.ROOF_CNST, CH.ROOMS,
		CH.BEDS, CH.BSMT, CH.BSMT_FIN, CH.HEAT, CH.OHEAT, CH.AIR, CH.ATTIC_TYPE,
		CH.ATTIC_FNSH, CH.CNST_QLTY, CH.RENOVATION, CH.SITE, CH.FBATH, CH.HBATH,
		CH.GAR1_SIZE, CH.PORCH, CH.BLDG_SF, CH.AGE
	FROM AS_DETAILT DT
	LEFT JOIN CCAOSFCHARS CH
		ON DT.PIN = CH.PIN 
		AND DT.TAX_YEAR = CH.TAX_YEAR 
		AND DT.DT_MLT_CD = CH.MULTI_CODE
)
SELECT TOP (1) DTC.*, 
  LEFT(T.HD_TOWN, 2) AS TOWN_CODE, 
  T.HD_NBHD AS NBHD, 
  T.HD_HD_SF AS HD_SF
FROM DTC
INNER JOIN AS_HEADT AS T
  ON T.PIN = DTC.PIN 
  AND T.TAX_YEAR = DTC.TAX_YEAR 
  AND T.HD_CLASS = DTC.CLASS 
WHERE T.PIN = {val1}
AND T.TAX_YEAR >= 2012
AND NOT (DTC.BEDS IS NULL AND DTC.CLASS != 299)
ORDER BY TAX_YEAR DESC